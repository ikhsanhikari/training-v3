package id.hikari.belajar.deploy.BelajarDeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarDeployApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarDeployApplication.class, args);
	}

}
